
def f ( n ):
    if n == 1:
        return lambda a, b: a * b
    else:
        return lambda a, b: a + b

def g ( n ):
    def aux ( a, b ):
        if n == 1:
            return a * b
        else:
            return a + b
    return aux

print( "\n play with f and g\n" )

# >>> g(1)(4,5)
#20
#>>> g(2)(4,5)
#9
#>>> g("don't concat")("con","cat")
#'concat'
#>>> f(1)(4,5)
#20
#>>> f(2)(4,5)
#9
#>>> f("don't concat")("con","cat")
#'concat'

