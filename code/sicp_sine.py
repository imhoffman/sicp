
def factorial ( n ):
    def fact_iter ( counter, accum ):
        if counter > n:
            return accum
        else:
            return fact_iter( counter+1, accum*counter )
    return fact_iter( 1, 1 )


def power ( base, exponent ):
    assert type(exponent) is int, "only integer enumerations of terms"
    def power_iter ( counter, accum ):
        if counter > exponent:
            return accum
        else:
            return power_iter( counter+1, accum*base )
    return power_iter( 1, 1 )


def sine ( angle, terms ):
    def sine_iter ( counter, accum ):
        if counter > terms:
            return accum
        else:
            term = 2*counter - 1
            return sine_iter(
                    counter+1,
                    accum + float(power(-1,counter+1))*power(angle,term)/factorial(term) )
    return sine_iter( 1, 0.0 )


print( sine( 3.14159/4.0, 9 ) )


