
;;  the tail-called, code-blocked version of Sec 1.2.2
;;   the Fib sequence is 1-indexed
;;   I count _up_ to `n` as in factorial rather than count backward as in SICP
(defn fibonacci [n]
  ((fn [b a counter]
     (if (or (= counter n) (< n 3))   ;; n>3 is best put second since `or` is lazy
       (case n
         1 b
         2 a
         a)    ;; obviously case `2` is this `else`, as in SICP, but this looks nicer to me
       (recur a (+ b a) (inc counter))))
   0 1 2))   ;;  initial state

(println (for [m (range 1 13)] (fibonacci m)))

