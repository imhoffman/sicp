!!
!!  Sec 1.2.2
!!
 module fibo
   implicit none
   contains

   recursive pure function fibo_iter ( b, a, counter, user_input ) result ( final_value )
     integer, intent(in) :: b, a, counter, user_input
     integer             :: final_value

     if ( counter .eq. user_input  .or.  user_input .lt. 3 ) then
       select case ( user_input )
         case ( 1 )
           final_value = b
         case default
           final_value = a
       end select
     else
       final_value = fibo_iter( a, b+a, counter+1, user_input )
     end if

     return
   end function fibo_iter

   !!  the called function for which the `iter` is a helper
   pure function fibonacci ( n )
     integer             :: fibonacci
     integer, intent(in) :: n

     fibonacci = fibo_iter( 0, 1, 2, n )

     return
   end function fibonacci
 end module fibo


!!
!!  main program
!!
 program main
  use fibo
  implicit none
  integer             :: m, i
  integer, parameter  :: n = 4
  character (len=n)   :: arg
  integer             :: fail = 0

  call get_command_argument(1,arg,status=fail)
  if ( fail .eq. 0 ) read(arg,*) m

  write(6,'(/,A)',advance='no') ' '
  do i = 1, m
    write(6,'(I0,A)',advance='no') fibonacci(i), ','
  end do
  write(6,'(/,A)') ''

  stop
 end program main

