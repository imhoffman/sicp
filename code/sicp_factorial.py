
def factorial ( n ):
    accum = 1
    counter = 1
    while counter <= n:
        accum = counter * accum
        counter = counter + 1
    return accum

#  a `while` formulation is immune from the stack limit
# >>> factorial(1200)
# ==> large integer


def factorial_recursive ( n, counter = 1, accum = 1 ):
    if counter > n:
        return accum
    else:
        return factorial_recursive( n, counter+1, accum*counter )

# >>> factorial_recursive(1200)
# ==> Guido

