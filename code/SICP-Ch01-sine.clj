
;;  Exercise 1.15
;;   actually a series expansion, not the 1/3 reduction from the book

(defn factorial [n]
  (#(if (> %1 n)
      %2
      (recur (inc %1) (* %1 %2)))
        1 1))


(defn power [base exponent]
  ((fn [counter accum]
     (if (> counter exponent)
       accum
       (recur (inc counter) (* accum base))))
   1 1))


(defn sine [angle terms]
  ((fn [counter accum]
     (if (> counter terms)
       accum
       (let [term-n (- (* counter 2) 1)]
         (recur
           (inc counter)
           (+ accum (*
                     (power -1 (inc counter))
                     (power angle term-n)
                     (/ 1 (factorial term-n))))))))
   1 0))


(println (sine (/ 3.14159 4.0) 9))

