
;;  `amount` should be an integer number of cents
;;  coin denominations are also given as 
;;   a list of integer cents
(defn number-of-ways-to-make-change [amount list-of-denominations]
  ((fn [remaining-amount remaining-denominations running-tally]
     (if (empty? remaining-denominations)
       running-tally
       (let [common-coin (peek remaining-denominations)]
         (recur
           (- amount common-coin)
           (pop remaining-denominations)
           (+ running-tally
              (if (= 0 (mod amount common-coin)) 1 0))))))  ;; dummy testing line
   amount list-of-denominations 0))


(println (number-of-ways-to-make-change 100 (list 1 5 10 25 50)))

