
 module types
   use iso_fortran_env
   implicit none
   integer, parameter :: iw = int64
   !integer, parameter :: iw = 16     ! ifort won't do 128-bit ints
 end module types

 module facto
   use types
   implicit none
   contains

   !!  fortran cannot do function-in-a-function
   !!  but with both the helper and the helpee
   !!  in a module, it is still a block box
   recursive pure function fact_iter ( accum, counter, user_input ) &
                  &  result ( final_value )
     integer ( kind=iw ), intent(in) :: accum, counter, user_input
     integer ( kind=iw )             :: final_value

     if ( counter .gt. user_input ) then
             final_value = accum
     else
             final_value = fact_iter( accum*counter, counter+1, user_input )
     end if

     return
   end function fact_iter

   !!  the called function for which the `iter` is a helper
   pure function factorial ( n )
     integer ( kind=iw )             :: factorial
     integer ( kind=iw ), intent(in) :: n

     factorial = fact_iter( int(1,kind=iw), int(1,kind=iw), n )

     return
   end function factorial
 end module facto


!!
!!  main program
!!
 program main
  use types
  use facto
  implicit none
  integer ( kind=iw ) :: m
  integer, parameter  :: n = 4
  character (len=n)   :: arg
  integer             :: fail = 0

  call get_command_argument(1,arg,status=fail)
  if ( fail .eq. 0 ) read(arg,*) m

  write(6,'(/,A,I2,A,I0,/)') ' ', m,'! = ', factorial(m)

  stop
 end program main

