
#  Exercise 1.12
#   using immutable tuples and recurrences (even though
#   Guido puts the tail calls on the stack anyway)

class Pascal:
    def __init__( self, n ):
        self.n = n
        return

    def line_iter( self, previous_line_work_stack, current_line ):
        if len( previous_line_work_stack ) == 1:
            return current_line + (1,)
        else:
            # `+` concatenates tuples, so a tuple literal is needed ... `(x,)`
            #  note the `,` at the end of the `current_line` concat
            #  the `+` between the previous_line entries is a simple sum
            return self.line_iter(
                        previous_line_work_stack[1:],
                        current_line +
                            ( previous_line_work_stack[0] + previous_line_work_stack[1], )
                        )

    def triangle_iter( self , line_number, coeffs ):
        if line_number > self.n:
            return coeffs
        else:
            return self.triangle_iter(
                        line_number+1,
                        self.line_iter( coeffs, (1,) )
                        )

    def triangle( self ):
        return self.triangle_iter( line_number = 1, coeffs = (1,1) )


#  print out several rows
for i in range(7):
    print( Pascal(i).triangle() )


