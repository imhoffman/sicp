
;;  as seen in HP lectures of SICP
;; https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-001-structure-and-interpretation-of-computer-programs-spring-2005/video-lectures/1a-overview-and-introduction-to-lisp/

;;  user must supply a sensible first guess as `latest-result`, a `latest-tolerance`
;;  that is greater than the ultimate `requested-tolerance`, and a counter of zero
(defn sqrt-imh [x requested-tolerance latest-result latest-tolerance counter]
  (if (< latest-tolerance requested-tolerance)
    (do
      (println)
      (println " converged within" latest-tolerance)
      (println " in" counter "iterations to")
      (println " square root of" x "is")
      (println " " latest-result)
      (println))
    (let [old-guess latest-result
          x-over-g  (/ x old-guess)
          new-guess (/ (+ old-guess x-over-g) 2)]
      (print \.)     ;; progress bar
      (recur
        x requested-tolerance      ;; maintain original inputs
        new-guess
        (Math/abs (- 1.0 (/ old-guess new-guess)))    ;; this is basically Exercise 1.7
        (inc counter)))))

;;
(sqrt-imh 29.0 0.25E-14 1.0 1.0 0)

;;  from SICP
;;  https://mitpress.mit.edu/sites/default/files/sicp/full-text/book/book-Z-H-10.html#%_sec_1.1.7
(defn average [x y]
  (/ (+ x y) 2.0))

(defn improve [guess x]
  (average guess (/ x guess)))

(defn good-enough? [guess x]
  (< (Math/abs (- (* guess guess) x)) 0.001))

(defn sqrt-iter [guess x]
  (if (good-enough? guess x)
    guess
    (recur (improve guess x) x)))

(defn sqrt [x]
  (sqrt-iter 1.0 x))

;;  Exercise 1.6
(defn new-if [pred then-clause else-clause]
  (case pred
    true then-clause
    else-clause))

(defn new-sqrt-iter [guess x]
  (new-if (good-enough? guess x)
    guess
    (new-sqrt-iter (improve guess x) x)))


