
;;  Exercise 1.12

(defn Pascals-triangle [n]
  ((fn [line-number coeffs]
     (if (> line-number n)
       coeffs
       (recur
         (inc line-number)
         ((fn [previous-line-work-stack current-line]
            (if (= 1 (count previous-line-work-stack))
              (conj current-line 1)
              (recur
                (vec (rest previous-line-work-stack))
                (conj
                  current-line
                  (+ (first previous-line-work-stack) (second previous-line-work-stack))))))
          coeffs (vector 1)))))
   1 (vector 1 1)))


(dotimes [i 7] (println (Pascals-triangle i)))

