#include<stdio.h>
#include<stdlib.h>

//  both gcc and icc will compile the
//  function-in-a-function below, but clang
//  complains; indeed, it is not C99
unsigned long long int factorial ( int n ) {

  unsigned long long int fact_iter ( unsigned long long int accum, int counter ) {
    if ( counter > n ) {
      return accum;
    } else {
      return fact_iter( accum * (unsigned long long int)counter, counter + 1 );
    }
  }

  //  `n` is in scope and need not be passed
  return fact_iter( 1ULL, 1 );
}


//  run as
//  $ ./a.out N
//  for N!
int main ( int argc, char* argv[] ) {

  int m = atoi( argv[1] );

  printf( "\n %d! = %llu\n\n", m, factorial(m) );

  return 0;
}

