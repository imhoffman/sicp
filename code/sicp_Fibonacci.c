#include<stdio.h>
#include<stdlib.h>

//  both gcc and icc will compile the
//  function-in-a-function below, but clang
//  complains; indeed, it is not C99
int fibonacci ( int n ) {

  int fibo_iter ( int b, int a, int counter ) {
    if ( ( counter == n ) || ( n < 3 ) ) {
      if ( n == 1 ) {     // rather than a case switch
	return b;
      } else {
	return a;
      }
    } else {
      return fibo_iter( a, b+a, counter+1 );
    }
  }
  //  I prefer to think of `b` as the larger of the two values...but SICP doesn't

  //  `n` is in scope and need not be passed
  //  this initial state will climb up to `n`
  return fibo_iter( 0, 1, 2 );
}


//  run as
//  $ ./a.out N
//  for the first N terms in the sequence
int main ( int argc, char* argv[] ) {

  int m = atoi( argv[1] );

  printf("\n ");
  for ( int i = 1; i <= m; i++ ) {
    printf( "%d,", fibonacci(i) );
  }
  printf("\n\n");

  return 0;
}

