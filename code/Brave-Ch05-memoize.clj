
(defn so-long [n]
  (dotimes [_ n] (Thread/sleep 1000))
  (str "Done running " n " long."))

(def memo-so-long (memoize so-long))


