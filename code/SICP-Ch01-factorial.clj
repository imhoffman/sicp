
;;  all of these allow `factorial` to be called on one argument
;;  the accumulator(s) are internal

;;  as per footnote 29
;;  %1 is the counter; %2 is the accumulator
(defn factorial [n]
  (#(if (> %1 n)
      %2
      (recur (inc %1) (* %1 %2)))
        1 1))


;;  how to bind the names??  must use `fn`, I think
(defn factorial [n]
  ((fn [counter accum]
     (if (> counter n)
       accum
       (recur (inc counter) (* counter accum))))
   1 1))


;;  or use overloading to initialize the accumulators
;;   https://clojure.org/about/functional_programming#_first_class_functions
;;   https://www.braveclojure.com/functional-programming/
(defn factorial
  ([n] (factorial n 1 1))
  ([n counter accum]
   (if (> counter n)
     accum
     (recur n (inc counter) (* counter accum)))))


;;  same as above, without the `counter` accumulator
(defn factorial
  ([n] (factorial n 1))
  ([n accum]
   (if (<= n 1)
     accum
     (recur (dec n) (* accum n)))))


