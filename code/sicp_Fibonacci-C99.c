#include<stdio.h>
#include<stdlib.h>

int facto_iter ( int b, int a, int counter, int n ) {
  switch ( n ) {
    case 1:
      return b;
      break;
    case 2:
      return a;
      break;
    default:
      if ( counter >= n ) {
       	return b+a;
      } else {
       	return facto_iter( a, b+a, counter+1, n );
      }
  }
}


int factorial ( int m ) {
  return facto_iter( 0, 1, 3, m );
}


int main ( int argc, char* argv[] ) {

  int m = atoi( argv[1] );

  printf( "\n " );
  for ( int i = 1; i <= m; i++ ) {
    printf( "%d,", factorial(i) );
  }
  printf( "\n\n" );

  return 0;
}


