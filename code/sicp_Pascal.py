
#  Exercise 1.12

class Pascal:
    def __init__( self, n ):
        self.n = n
        return

    def line_iter( self, previous_line, current_line ):
        # making a copy of the argument here
        # could avoid a copy with an incremented address index
        # i.e., `i` and `i+1` rather than `0` and `1` in the `append`
        # and then `previous_line` could be a tuple
        scoped_previous_line = []                  
        scoped_previous_line[:] = previous_line[:] 
        while len( scoped_previous_line) != 1:
            current_line.append( scoped_previous_line[0] + scoped_previous_line[1] )
            scoped_previous_line = scoped_previous_line[1:]
        current_line.append(1)
        return current_line

    def triangle_iter( self , line_number = 1, coeffs = [ 1, 1 ] ):
        # must iterate rather than tail-call in order to avoid Guido limit
        while line_number <= self.n:
            line_number = line_number + 1
            old_coeffs = []
            old_coeffs[:] = coeffs[:]
            coeffs = self.line_iter( old_coeffs, [1] )
        return coeffs

    def triangle( self ):
        return self.triangle_iter()


for i in range(7):
    coefficients = Pascal(i)
    print( coefficients.triangle() )


