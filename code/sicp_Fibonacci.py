
#  https://stackoverflow.com/questions/481692/can-a-lambda-function-call-itself-recursively-in-python

Y = (lambda f: (lambda x: f(lambda v: x(x)(v)))(lambda x: f(lambda v: x(x)(v))))

fibo = (lambda f: (lambda i: f(i - 1) + f(i - 2) if i > 1 else 1))

Fibonacci = Y( fibo )

[ print( Fibonacci(n), end=', ', flush=True ) for n in range(12) ]
print()

